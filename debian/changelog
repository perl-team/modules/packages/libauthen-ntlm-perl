libauthen-ntlm-perl (1.09-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright:
    + migrate pre-1.0 format to 1.0 using "cme fix dpkg-copyright"
    + Change Mark Bush's e-mail address to his cpan.org address. (Issue
      reported by DUCK.)
  * Change CPAN Distribution name from Authen-NTLM to NTLM in all MetaCPAN
    URLs and in the Upstream-Name field in debian/copyright. The main
    module's name is Authen::NTLM, but the CPAN Distribution's name is
    just NTML. Found by DUCK.

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team. Closes: #924980
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Bump debhelper from old 12 to 13.
  * Apply multi-arch hints.
    + libauthen-ntlm-perl: Add Multi-Arch: foreign.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Nov 2024 17:58:53 +0100

libauthen-ntlm-perl (1.09-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 18:18:57 +0100

libauthen-ntlm-perl (1.09-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nicholas Bamber ]
  * New upstream release
  * Raised standards version to 3.9.2

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 11 Sep 2011 09:04:13 +0100

libauthen-ntlm-perl (1.08-1) unstable; urgency=low

  * Initial Release. (Closes: #507669)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 09 Apr 2011 11:37:02 +0100
